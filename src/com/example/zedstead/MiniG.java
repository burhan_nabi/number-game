package com.example.zedstead;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class MiniG extends Activity {

	int moves;
	Random ran;
	int result, random;
	Button Sub, Square, Mul, Div, Cube, Add, Reset, RTZ;
	TextView showMoves, showResult, showTarget;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.game);

		moves = 0;
		result = 0;
		random = 0;
		ran = new Random();
		random = ran.nextInt(989) + 11;

		Square = (Button) findViewById(R.id.bSquare);
		Cube = (Button) findViewById(R.id.bCube);
		Add = (Button) findViewById(R.id.bAdd);
		Sub = (Button) findViewById(R.id.bSub);
		Reset = (Button) findViewById(R.id.bReset);
		RTZ = (Button) findViewById(R.id.bRTZ);
		Mul = (Button) findViewById(R.id.bMul);
		Div = (Button) findViewById(R.id.bDiv);

		// showMoves=(TextView) findViewById(R.id.tMoves);
		showResult = (TextView) findViewById(R.id.tResult);
		showTarget = (TextView) findViewById(R.id.tTarget);

		showTarget.setText("Target:" + random);

		Add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				result++;
				moves++;
				// showMoves.setText("Moves: "+moves);
				showResult.setText("=>" + result);

				if (result == random) {
					try {

						Class win = Class.forName("com.example.zedstead.Win");
						Intent winIntent = new Intent(MiniG.this, win);
						startActivity(winIntent);

					} catch (ClassNotFoundException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}

				else if (moves == 15) {

					try {
						Class newClass = Class
								.forName("com.example.zedstead.Lost");
						Intent newIntent = new Intent(MiniG.this, newClass);
						startActivity(newIntent);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});

		Sub.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				result--;
				moves++;
				// showMoves.setText("Moves: "+moves);
				showResult.setText("=>" + result);

				if (result == random) {
					try {

						Class win = Class.forName("com.example.zedstead.Win");
						Intent winIntent = new Intent(MiniG.this, win);
						startActivity(winIntent);

					} catch (ClassNotFoundException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}

				else if (moves == 15) {

					try {
						Class newClass = Class
								.forName("com.example.zedstead.Lost");
						Intent newIntent = new Intent(MiniG.this, newClass);
						startActivity(newIntent);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});

		Mul.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				result *= 2;
				moves++;
				// showMoves.setText("Moves: "+moves);
				showResult.setText("=>" + result);

				if (result == random) {
					try {

						Class win = Class.forName("com.example.zedstead.Win");
						Intent winIntent = new Intent(MiniG.this, win);
						startActivity(winIntent);

					} catch (ClassNotFoundException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}

				else if (moves == 15) {

					try {
						Class newClass = Class
								.forName("com.example.zedstead.Lost");
						Intent newIntent = new Intent(MiniG.this, newClass);
						startActivity(newIntent);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});

		Div.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				result /= 2;
				moves++;
				// showMoves.setText("Moves: "+moves);
				showResult.setText("=>" + result);

				if (result == random) {
					try {

						Class win = Class.forName("com.example.zedstead.Win");
						Intent winIntent = new Intent(MiniG.this, win);
						startActivity(winIntent);

					} catch (ClassNotFoundException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}

				else if (moves == 15) {

					try {
						Class newClass = Class
								.forName("com.example.zedstead.Lost");
						Intent newIntent = new Intent(MiniG.this, newClass);
						startActivity(newIntent);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});

		Square.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				moves++;
				result *= result;
				// showMoves.setText("Moves: "+moves);
				showResult.setText("=>" + result);

				if (result == random) {
					try {

						Class win = Class.forName("com.example.zedstead.Win");
						Intent winIntent = new Intent(MiniG.this, win);
						startActivity(winIntent);

					} catch (ClassNotFoundException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}

				else if (moves == 15) {

					try {
						Class newClass = Class
								.forName("com.example.zedstead.Lost");
						Intent newIntent = new Intent(MiniG.this, newClass);
						startActivity(newIntent);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		Cube.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				moves++;
				result *= result * result;
				// showMoves.setText("Moves: "+moves);
				showResult.setText("=>" + result);

				if (result == random) {
					try {

						Class win = Class.forName("com.example.zedstead.Win");
						Intent winIntent = new Intent(MiniG.this, win);
						startActivity(winIntent);

					} catch (ClassNotFoundException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}

				else if (moves == 15) {

					try {
						Class newClass = Class
								.forName("com.example.zedstead.Lost");
						Intent newIntent = new Intent(MiniG.this, newClass);
						startActivity(newIntent);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		Reset.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Class myClass;
				try {
					myClass = Class.forName("com.example.zedstead.MiniG");
					Intent myIntent = new Intent(MiniG.this, myClass);
					startActivity(myIntent);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		RTZ.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				result = 0;
				showResult.setText("=>" + result);

			}
		});

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}
