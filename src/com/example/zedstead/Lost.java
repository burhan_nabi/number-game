package com.example.zedstead;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Lost extends Activity{
	
	
	Button PlayAgain;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.lost);
		
		PlayAgain=(Button) findViewById(R.id.PlayAgain);
		
		PlayAgain.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				try {
				
					Class myClass = Class.forName("com.example.zedstead.MiniG");
					Intent myIntent=new Intent(Lost.this,myClass);
					startActivity(myIntent);
					
				} catch (ClassNotFoundException e) {
				
					e.printStackTrace();
					// TODO: handle exception
				}
				
				
				
			}
		});
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}
