package com.example.zedstead;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;



public class Welcome extends Activity{

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		
		
		Thread timer=new Thread(){
			public void run() {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO: handle exception
					e.fillInStackTrace();
				}finally{
					Intent newIntent=new Intent("com.example.zedstead.MENU");
					startActivity(newIntent);
				}
			};
		};
		timer.start();
	
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	
	
}
